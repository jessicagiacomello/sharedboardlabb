package Client;

import Server.RmiObservableService_server;
import Server.User;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Vector;

public class DeleteYourBoards extends JDialog {

    /**
     *
     */
    public static String board;
    static JTable table;
    int index;
    RmiObservableService_server remoteService;
    private final User u;

    public DeleteYourBoards(RmiObservableService_server stub, User user) throws SQLException, RemoteException {
        this.u = user;
        remoteService = stub;

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setTitle("Elimina le tue lavagne dal DB");
        setAutoRequestFocus(false);

        setResizable(false);

        JPanel topPanel = new JPanel();

        JLabel label1 = new JLabel("Lavagne di tua propriet\u00E0");
        label1.setPreferredSize(new Dimension(200, 30));

        // JLabel label2 = new JLabel("Tabella riferimento SQL:  Lavagna");
        // label2.setPreferredSize(new Dimension(200, 30));

        topPanel.add(label1);
        // topPanel.add(label2);

        getContentPane().add(topPanel, BorderLayout.NORTH);

        Vector<String> headers = new Vector<>();
        headers.add("Nome Lavagna");
        headers.add("Data Creazione");

        Vector<Vector<String>> myboards = remoteService.getYourBoards(u);
        // Metodo che restituisce tutti gli utenti presenti nel database e popola la JTable

        final DefaultTableModel model = new DefaultTableModel(myboards, headers);
        table = new JTable(model);
        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                index = table.getSelectedRow();
                if (index == -1) {
                    return;
                }
                board = table.getModel().getValueAt(index, 0).toString();

            }
        });

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        header_size();

        JScrollPane scroll = new JScrollPane(table);

        scroll.setHorizontalScrollBarPolicy(
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(
                JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);


        getContentPane().add(scroll, BorderLayout.EAST);
        pack();
        setVisible(true);

        JPanel underTop = new JPanel();
        JButton Confirm = new JButton("Conferma");
        Confirm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (table.getSelectedRow() != -1) {
                    try {
                        int reply = JOptionPane.showConfirmDialog(null, "Sicuro di voler eliminare la lavagna " + board + " ?", "Attenzione", JOptionPane.YES_NO_OPTION);
                        if (reply == JOptionPane.YES_OPTION) {
                            model.removeRow(index);
                            remoteService.deleteYourBoard(u, board);
                            model.fireTableDataChanged();
                            JOptionPane.showMessageDialog(null, "La Lavagna " + board + " e' stata eliminata.");
                            table.clearSelection();
                        }
                    } catch (RuntimeException | RemoteException | SQLException e) {
                        e.printStackTrace();
                    }
                } else
                    JOptionPane.showMessageDialog(null, "Non hai selezionato alcuna lavagna in questo momento.", "Attenzione", JOptionPane.WARNING_MESSAGE);
            }
        });

        JButton Cancel = new JButton("Indietro");
        Cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        Confirm.setBounds(100, 100, 100, 100);
        Cancel.setBounds(100, 100, 100, 100);

        underTop.add(Confirm);
        underTop.add(Cancel);
        getContentPane().add(underTop, BorderLayout.SOUTH);
    }

    /**
     * Setting the particular Column Size in JTable
     */
    public static void header_size() {
        TableColumn column = table.getColumnModel().getColumn(0);
        column.setPreferredWidth(100);
        column = table.getColumnModel().getColumn(1);
        column.setPreferredWidth(100);
    }
} 
