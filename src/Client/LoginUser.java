package Client;

import Server.RmiObservableService_server;
import Server.UserNotFoundException;
import Server.WrongPasswordException;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

public class LoginUser extends JDialog {

    /**
     *
     */
    private final JPanel contentPanel = new JPanel();
    private JTextField nu;
    private JPasswordField pw;

    String username, password;
    RmiObservableService_server remoteService;

    /**
     * Create the dialog.
     *
     * @param stub
     */
    public LoginUser(RmiObservableService_server stub) {
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/login.png"));

        remoteService = stub;

        setTitle("Login User");
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);
        {
            JLabel lblNomeUtente = new JLabel("Nome Utente");
            lblNomeUtente.setBounds(33, 37, 119, 14);
            contentPanel.add(lblNomeUtente);
        }
        {
            JLabel lblPassword = new JLabel("Password");
            lblPassword.setBounds(33, 77, 119, 14);
            contentPanel.add(lblPassword);
        }
        {
            nu = new JTextField();
            nu.setBounds(217, 34, 163, 20);
            contentPanel.add(nu);
            nu.setColumns(10);
        }
        {
            pw = new JPasswordField();
            pw.setBounds(217, 74, 163, 20);
            contentPanel.add(pw);
        }
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if ((!nu.getText().equals("")) || (!pw.getText().equals(""))) {
                            username = nu.getText();
                            password = pw.getText();
                        }
                        try {
                            // Login
                            if (remoteService.login(username, password)) {
                                Client_GUI.user = remoteService.GetUser(username);
                                Client_GUI.lblUsername.setText("Utente attivo: " + username);
                                JOptionPane.showMessageDialog(null, "Autenticazione riuscita " + "\n" + "Benvenuto " + username, "Accesso effettuato", JOptionPane.INFORMATION_MESSAGE);
                                Client_GUI.CreateWB.setEnabled(true);
                                Client_GUI.DeleteWB.setEnabled(true);
                                Client_GUI.Request.setEnabled(true);
                                Client_GUI.Invited.setEnabled(true);
                                Client_GUI.Access.setEnabled(true);
                                Client_GUI.Statistics.setEnabled(true);
                                Client_GUI.Register.setEnabled(false);
                                Client_GUI.Confirm.setEnabled(false);
                                Client_GUI.Login.setEnabled(false);
                                Client_GUI.Logout.setEnabled(true);
                            } else {
                                Client_GUI.CreateWB.setEnabled(false);
                                Client_GUI.DeleteWB.setEnabled(false);
                                Client_GUI.Request.setEnabled(false);
                                Client_GUI.Invited.setEnabled(false);
                                Client_GUI.Access.setEnabled(false);
                                Client_GUI.Statistics.setEnabled(false);
                                Client_GUI.Register.setEnabled(true);
                                Client_GUI.Confirm.setEnabled(true);
                                Client_GUI.Login.setEnabled(true);
                                JOptionPane.showMessageDialog(null, "Attenzione, utenza non ancora confermata. Controlla la tua casella di posta.", "Attenzione", JOptionPane.INFORMATION_MESSAGE);
                            }
                            dispose();
                        } catch (HeadlessException | NullPointerException | IOException | SQLException e1) {
                            e1.printStackTrace();
                        } catch (UserNotFoundException | WrongPasswordException e2) {
                            JOptionPane.showMessageDialog(null, "Utente o Password errati.");
                        }
                    }
                });

                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton("Annulla");
                cancelButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Client_GUI.CreateWB.setEnabled(false);
                        Client_GUI.DeleteWB.setEnabled(false);
                        Client_GUI.Request.setEnabled(false);
                        Client_GUI.Invited.setEnabled(false);
                        Client_GUI.Access.setEnabled(false);
                        Client_GUI.Statistics.setEnabled(false);
                        Client_GUI.Register.setEnabled(true);
                        Client_GUI.Confirm.setEnabled(true);
                        Client_GUI.Login.setEnabled(true);
                        dispose();
                    }
                });
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }
    }
}