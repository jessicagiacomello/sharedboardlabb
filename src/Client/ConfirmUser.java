package Client;

import Server.RmiObservableService_server;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

public class ConfirmUser extends JDialog {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final JPanel contentPanel = new JPanel();
    private JTextField nu;
    private JPasswordField pw;
    private JTextField captcha;

    String username, password;
    String cap;
    RmiObservableService_server remoteService;

    /**
     * Create the dialog.
     *
     */
    public ConfirmUser(RmiObservableService_server stub) {
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/confirm.png"));

        remoteService = stub;

        setTitle("Confirm User");
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);

        JLabel lblNomeUtente = new JLabel("Nome Utente");
        lblNomeUtente.setBounds(42, 37, 105, 14);
        contentPanel.add(lblNomeUtente);

        JLabel lblPassword = new JLabel("Password");
        lblPassword.setBounds(42, 71, 105, 14);
        contentPanel.add(lblPassword);

        JLabel lblCaptcha = new JLabel("Captcha");
        lblCaptcha.setBounds(42, 114, 105, 14);
        contentPanel.add(lblCaptcha);

        nu = new JTextField();
        nu.setBounds(194, 34, 158, 20);
        contentPanel.add(nu);
        nu.setColumns(10);

        pw = new JPasswordField();
        pw.setBounds(194, 68, 158, 20);
        contentPanel.add(pw);

        captcha = new JTextField();
        captcha.setBounds(194, 111, 158, 20);
        contentPanel.add(captcha);
        captcha.setColumns(10);
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (!nu.getText().equals("") && !pw.getText().equals("") && !captcha.getText().equals("")) {
                            username = nu.getText();
                            password = pw.getText();
                            cap = captcha.getText();

                            try {

                                if (remoteService.returnCaptcha(cap, username, password)) {
                                    remoteService.confirm(username, password, cap);
                                    JOptionPane.showMessageDialog(null, "Account confermato correttamente.");
                                } else
                                    JOptionPane.showMessageDialog(null, "Attenzione, credenziali o captcha errati.", "Attenzione", JOptionPane.WARNING_MESSAGE);
                            } catch (HeadlessException | NullPointerException | IOException | SQLException e1) {
                                e1.printStackTrace();
                            }


                            dispose();

                            if (nu.getText().equals("") || pw.getText().equals("") || captcha.getText().equals(""))
                                JOptionPane.showMessageDialog(null, "Attenzione, credenziali o captcha errati.", "Attenzione", JOptionPane.WARNING_MESSAGE);
                        }
                    }
                });
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton("Cancel");
                cancelButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        dispose();
                    }
                });
                cancelButton.setActionCommand("Cancel");
                buttonPane.add(cancelButton);
            }
        }
    }
}
