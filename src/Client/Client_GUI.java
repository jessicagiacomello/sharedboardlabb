package Client;

import Server.RemoteObserver_client;
import Server.RmiObservableService_server;
import Server.User;
import Whiteboard.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;


public class Client_GUI extends UnicastRemoteObject implements RemoteObserver_client {

    /**
     *
     */
    JFrame frmClientgui;

    static ArrayList<WhiteBoard> wb_list_client; // Tutte le lavagne che ha attive l utente
    public static User user;

    static JButton Confirm, Register, Login, CreateWB, DeleteWB, Request, Invited, Logout, Exit, Access, Statistics;

    private static RmiObservableService_server stub;
    private JLabel lblNewLabel;
    public static JLabel lblUsername;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    wb_list_client = new ArrayList<>();
                    Registry registry = LocateRegistry.getRegistry(1099);
                    stub = (RmiObservableService_server) registry.lookup("RmiService");

                    Client_GUI window = new Client_GUI();
                    window.frmClientgui.setVisible(true);

                    stub.addObserver(window);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void OpenBoard(boolean isHost, String name) {
        WhiteBoard wb;
        final String Name_to_erase = name;
        String check;
        if (isHost) {
            try {
                Date date = new Date(System.currentTimeMillis());

                // Creo un nuovo riferimento di lavagna e lo passo al wb server
                Board board = new Board(name, date, user.getUsername());
                check = stub.InsertBoard(board); // Insert new board

                if (!(check.equals("esistente"))) {
                    wb = new WhiteBoardServer(board, stub);
                    wb_list_client.add(wb);
                    wb.setVisible(true);
                    wb.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                    wb.addWindowListener(new WindowListener() {
                        public void windowClosed(WindowEvent arg0) {
                            if (wb_list_client.size() > 0) {
                                for (int i = 0; i < wb_list_client.size(); i++) {
                                    if (wb_list_client.get(i).getBoard_name().equals(Name_to_erase)) ;
                                    wb_list_client.remove(i);
                                }
                            }
                        }

                        @Override
                        public void windowActivated(WindowEvent arg0) {
                        }

                        @Override
                        public void windowClosing(WindowEvent arg0) {
                        }

                        @Override
                        public void windowDeactivated(WindowEvent arg0) {
                        }

                        @Override
                        public void windowDeiconified(WindowEvent arg0) {
                        }

                        @Override
                        public void windowIconified(WindowEvent arg0) {
                        }

                        @Override
                        public void windowOpened(WindowEvent arg0) {
                        }
                    });
                } else {
                    JOptionPane.showMessageDialog(null, "Nome lavagna non disponibile.");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                wb = new WhiteBoardClient(name, stub);
                wb_list_client.add(wb);
                wb.setVisible(true);
                wb.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                wb.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
                wb.addWindowListener(new WindowListener() {
                    public void windowClosed(WindowEvent arg0) {
                        for (int i = 0; i < wb_list_client.size(); i++) {
                            if (wb_list_client.get(i).getBoard_name().equals(Name_to_erase)) ;
                            wb_list_client.remove(i);
                        }
                    }

                    @Override
                    public void windowActivated(WindowEvent arg0) {
                    }

                    @Override
                    public void windowClosing(WindowEvent arg0) {
                    }

                    @Override
                    public void windowDeactivated(WindowEvent arg0) {
                    }

                    @Override
                    public void windowDeiconified(WindowEvent arg0) {
                    }

                    @Override
                    public void windowIconified(WindowEvent arg0) {
                    }

                    @Override
                    public void windowOpened(WindowEvent arg0) {
                    }
                });

                ArrayList<Segment> old_segment_of_wb = stub.getSegments(name);

                for (int i = 0; i < old_segment_of_wb.size(); i++) {
                    wb.List_Client_Segment.add(old_segment_of_wb.get(i)); // Aggiunge segmento a lista
                    wb.paper.drawSegment(old_segment_of_wb.get(i));// Lo stampa sul pannello
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Create the application.
     *
     * @throws RemoteException
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    public Client_GUI() throws RemoteException, ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        super();
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     *
     * @throws UnsupportedLookAndFeelException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws ClassNotFoundException
     */
    private void initialize() throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        frmClientgui = new JFrame();
        frmClientgui.setIconImage(Toolkit.getDefaultToolkit().getImage("images/pencil.png"));
        frmClientgui.setTitle("Sharedboard");
        frmClientgui.setBounds(100, 100, 605, 390);
        frmClientgui.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmClientgui.getContentPane().setLayout(null);

        Register = new JButton("Registra Utente");
        Register.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    InsertUser dialog = new InsertUser(stub);
                    dialog.setTitle("Registrazione");
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Register.setBounds(300, 50, 145, 25);
        frmClientgui.getContentPane().add(Register);

        Confirm = new JButton("Conferma Utente");
        Confirm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    ConfirmUser dialog = new ConfirmUser(stub);
                    dialog.setTitle("Conferma Utente");
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        Confirm.setBounds(445, 50, 145, 25);
        frmClientgui.getContentPane().add(Confirm);

        Login = new JButton("Login");
        Login.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    LoginUser dialog = new LoginUser(stub);
                    dialog.setTitle("Login");
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception o) {
                    o.printStackTrace();
                }
            }
        });

        Login.setBounds(300, 85, 290, 25);
        frmClientgui.getContentPane().add(Login);

        Exit = new JButton("Esci");
        Exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int reply = JOptionPane.showConfirmDialog(null, "Confermi di voler uscire?", "Esci", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (reply == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
            }
        });

        Exit.setBounds(450, 265, 130, 25);
        frmClientgui.getContentPane().add(Exit);

        CreateWB = new JButton("Crea Lavagna");
        CreateWB.setEnabled(false);
        CreateWB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = JOptionPane.showInputDialog(null, "Inserire nome Lavagna: ");
                if (!(name == null || (name != null && ("".equals(name))))) {
                    OpenBoard(true, name);
                }
            }
        });

        CreateWB.setBounds(300, 165, 140, 25);
        frmClientgui.getContentPane().add(CreateWB);

        DeleteWB = new JButton("Elimina Lavagne");
        DeleteWB.setEnabled(false);
        DeleteWB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    DeleteYourBoards dialog = new DeleteYourBoards(stub, user);
                    dialog.setTitle("Elimina Lavagne");
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception o) {
                    o.printStackTrace();
                }
            }
        });

        DeleteWB.setBounds(445, 165, 135, 25);
        frmClientgui.getContentPane().add(DeleteWB);

        Request = new JButton("Richiedi Invito");
        Request.setEnabled(false);
        Request.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    RequestPartecipation dialog = new RequestPartecipation(stub);
                    dialog.setTitle("Richiedi Invito");
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception o) {
                    o.printStackTrace();
                }
            }
        });

        Request.setBounds(445, 200, 135, 25);
        frmClientgui.getContentPane().add(Request);

        Invited = new JButton("Invita Persone");
        Invited.setEnabled(false);
        Invited.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    InvitedPeople dialog = new InvitedPeople(stub);
                    dialog.setTitle("Invita Persone");
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception o) {
                    o.printStackTrace();
                }
            }
        });

        Invited.setBounds(300, 200, 140, 25);
        frmClientgui.getContentPane().add(Invited);

        Statistics = new JButton("Statistiche");
        Statistics.setEnabled(false);
        Statistics.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    Statistics dialog = new Statistics(stub);
                    dialog.setTitle("Statistiche");
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception o) {
                    o.printStackTrace();
                }
            }
        });

        Statistics.setBounds(365, 310, 145, 25);
        frmClientgui.getContentPane().add(Statistics);

        Access = new JButton("Accedi a lavagna da invito");
        Access.setEnabled(false);
        Access.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String nameBoard = JOptionPane.showInputDialog(null, "Nome lavagna a cui vuoi accedere: ");
                if (!(nameBoard == null || nameBoard.equals(""))) {  // Controllo se non inserisce nulla o se torna indietro
                    try {
                        if (stub.checkExistingBoard(nameBoard)) {
                            OpenBoard(false, nameBoard);
                        } else {
                            JOptionPane.showMessageDialog(null, "Lavagna non disponibile", "Attenzione", JOptionPane.INFORMATION_MESSAGE);
                        }
                    } catch (SQLException | RemoteException throwables) {
                        throwables.printStackTrace();
                    }
                }
            }
        });

        Access.setBounds(300, 230, 280, 25);
        frmClientgui.getContentPane().add(Access);

        Logout = new JButton("Logout");
        Logout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                CreateWB.setEnabled(false);
                DeleteWB.setEnabled(false);
                Request.setEnabled(false);
                Invited.setEnabled(false);
                Access.setEnabled(false);
                Statistics.setEnabled(false);
                Register.setEnabled(true);
                Confirm.setEnabled(true);
                Login.setEnabled(true);
                Logout.setEnabled(false);
                user = null;
                lblUsername.setText("");
            }
        });

        Logout.setBounds(300, 265, 120, 25);
        frmClientgui.getContentPane().add(Logout);
        Logout.setEnabled(false);

        lblNewLabel = new JLabel();
        lblNewLabel.setIcon(new ImageIcon("images/whiteboard.png"));
        lblNewLabel.setBounds(20, 35, 300, 250);
        frmClientgui.getContentPane().add(lblNewLabel);

        lblUsername = new JLabel("");
        lblUsername.setBounds(20, 250, 195, 25);
        frmClientgui.getContentPane().add(lblUsername);
    }

    @Override
    public void remoteUpdate(Object observable, Object updateMsg) {
        /* Scansiono l'array di lavagne dell'utente, se il segmento ricevuto fa parte di una delle lavagne dell'utente
         * e se la lavagna trovata è di tipo server o di tipo client riceve il segmento e lo scrive sulla lavagna */

        //qui se l'object è istanza di segmento fai l'update per segmento, altrimenti, se è un array devi fare una redo screen
        if (updateMsg instanceof Segment) {
            Segment s = (Segment) updateMsg; // Stampa segmento ricevuto
           // System.out.println("segmento ricevuto dal client(Username): " + s.getUsername() + " data: " + s.getCreation() +
                    // " Nome_lav: " + s.getBoardname());

            for (int i = 0; i < wb_list_client.size(); i++) {
                if (wb_list_client.get(i).getBoard_name().equals(s.getBoardname()))
                    if (wb_list_client.get(i) instanceof WhiteBoardServer)
                        wb_list_client.get(i).receiveSegment(s, true);
                    else
                        wb_list_client.get(i).receiveSegment(s, true);
            }
        } else if (updateMsg instanceof String) {
            /* Remote upadate della lavagna con il nome della lavagna chiamandola in wb_server
             * tramite il nome della lavagna ricevuto la puoi cercare tra quelle dell' utente
             * poi sempre a seconda se � client o server chiamo reset board
             */
            String name = (String) updateMsg;

            // String[] params = name.split(",");
            for (int y = 0; y < wb_list_client.size(); y++) {
                if (wb_list_client.get(y).getBoard_name().equals(name))
                    if (wb_list_client.get(y) instanceof WhiteBoardServer)
                        wb_list_client.get(y).reset_board();
                    else
                        wb_list_client.get(y).reset_board();
            }
        } else {
            ArrayList<Segment> list = (ArrayList<Segment>) updateMsg;
            if (list.isEmpty()) {
                System.out.println("la lista è vuota.");
            }
            for (int y = 0; y < wb_list_client.size(); y++) {
                if (wb_list_client.get(y).getBoard_name().equals(list.get(0).getBoardname()))
                    if (wb_list_client.get(y) instanceof WhiteBoardServer)
                        wb_list_client.get(y).receiveSegments(list);

                    else
                        wb_list_client.get(y).receiveSegments(list);
            }
        }
    }
}