package Client;

import Server.RmiObservableService_server;
import Server.SendEmail;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Vector;

public class RequestPartecipation extends JDialog {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    static JTable table;
    public static String board, email;
    int index;
    private String line1;
    private String line2;

    RmiObservableService_server remoteService;

    public RequestPartecipation(RmiObservableService_server stub) throws SQLException {
        remoteService = stub;

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setTitle("Cerca Lavagne attive dal DB");
        setAutoRequestFocus(false);
        setResizable(false);

        JPanel topPanel = new JPanel();

        JLabel label1 = new JLabel("Cerca Lavagne attive dal DB");
        label1.setPreferredSize(new Dimension(200, 30));

        //JLabel label2 = new JLabel("MySQL Table Name     :  Lavagna");
        // label2.setPreferredSize(new Dimension(200, 30));

        topPanel.add(label1);
        // topPanel.add(label2);

        getContentPane().add(topPanel, BorderLayout.NORTH);

        Vector<String> headers = new Vector<>();
        headers.add("Nome Lavagna");
        headers.add("Data Creazione");
        headers.add("Propretario");
        headers.add("Email");


        Vector<Vector<String>> boards = new Vector<>();
        try {
            boards = remoteService.getActiveBoards(Client_GUI.user.getUsername());
        } catch (RemoteException e1) {
            e1.printStackTrace();
        }

        final DefaultTableModel model = new DefaultTableModel(boards, headers);

        table = new JTable(model);
        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                index = table.getSelectedRow();
                if (index == -1) {
                    return;
                }
                board = table.getModel().getValueAt(index, 0).toString();
                System.err.println("Lavagna scelta: " + board);
                email = table.getModel().getValueAt(index, 3).toString();
                System.err.println("Email creatore " + email);
            }
        });

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        header_size();

        JScrollPane scroll = new JScrollPane(table);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        getContentPane().add(scroll, BorderLayout.EAST);
        pack();
        setVisible(true);

        line1 = "Gentile utente," + "\n";
        line2 = "l'utente " + Client_GUI.user.getUsername() + " vorrebbe essere invitato a partecipare alla tua lavagna chiamata \"";

        JPanel underTop = new JPanel();
        JButton Confirm = new JButton("Conferma");
        Confirm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                String mail = JOptionPane.showInputDialog("Inserire email del creatore della lavagna: ", email);
                SendEmail se = new SendEmail(mail);
                se.setSubject("Richiesta di partecipazione a lavagna");
                String txt = line1 + line2 + board + "\"";
                se.setText(txt);
                se.send();
            }
        });
        JButton Cancel = new JButton("Cancella");
        Cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        Confirm.setBounds(100, 100, 100, 100);
        Cancel.setBounds(100, 100, 100, 100);

        underTop.add(Confirm);
        underTop.add(Cancel);
        getContentPane().add(underTop, BorderLayout.SOUTH);
    }

    /**
     * Setting the particular Column Size in JTable
     */
    public static void header_size() {
        TableColumn column = table.getColumnModel().getColumn(0);
        column.setPreferredWidth(100);

        column = table.getColumnModel().getColumn(1);
        column.setPreferredWidth(100);

        column = table.getColumnModel().getColumn(2);
        column.setPreferredWidth(100);

        column = table.getColumnModel().getColumn(3);
        column.setPreferredWidth(150);
    }
} 
