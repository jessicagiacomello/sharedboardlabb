package Client;

import Server.RmiObservableService_server;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Statistics extends JDialog {

    /**
     *
     */
    private final JPanel contentPanel = new JPanel();

    RmiObservableService_server remoteService;
    static Date inizio, fine;
    private SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");

    /**
     * Create the dialog.
     */
    public Statistics(RmiObservableService_server stub) {

        remoteService = stub;
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/statistics.png"));
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setTitle("Statistiche");
        setBounds(100, 100, 425, 260);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);

        JButton buttonQuery1 = new JButton("Attività Utenti");
        buttonQuery1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    StatisticsForUsers dialog = new StatisticsForUsers(remoteService, Client_GUI.user, null, null);
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception o) {
                    o.printStackTrace();
                }
            }
        });

        buttonQuery1.setToolTipText("Riporta le statistiche di attività sulle tue lavagne.");
        buttonQuery1.setBounds(10, 90, 230, 25);
        contentPanel.add(buttonQuery1);

        JButton buttonQuery2 = new JButton("Tratti in Intervallo di Tempo");
        buttonQuery2.setToolTipText("Restituisce quanti tratti sono stati tracciati nelle tue lavagne in un determinato intervallo di tempo.");
        buttonQuery2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                String dataInizio = JOptionPane.showInputDialog("Inserire data inizio calcolo nel formato gg/mm/aaaa:", null);
                try {
                    java.util.Date parsed = sdfDate.parse(dataInizio);
                    inizio = new Date(parsed.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                System.out.println("DATA INIZIO: " + inizio);

                String dataFine = JOptionPane.showInputDialog("Inserire data fine calcolo nel formato gg/mm/aaaa: ", null);
                try {
                    java.util.Date parsed = sdfDate.parse(dataFine);
                    fine = new Date(parsed.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                System.out.println("DATA FINE: " + fine);

                try {
                    StatisticsForUsers dialog = new StatisticsForUsers(remoteService, Client_GUI.user, inizio, fine);
                    dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                    dialog.setVisible(true);
                } catch (Exception o) {
                    o.printStackTrace();
                }
            }
        });
        buttonQuery2.setBounds(10, 125, 230, 25);
        contentPanel.add(buttonQuery2);

        JPanel buttonPane = new JPanel();
        buttonPane.setBounds(85, 190, 300, 30);
        contentPanel.add(buttonPane);
        buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));

        JButton cancelButton = new JButton("Annulla");
        cancelButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                dispose();
            }
        });
        cancelButton.setActionCommand("Cancel");
        buttonPane.add(cancelButton);

        JLabel lblStatistiche = new JLabel("Statistiche");
        lblStatistiche.setFont(new Font("Andale Mono", Font.BOLD, 24));
        lblStatistiche.setForeground(Color.BLACK);
        lblStatistiche.setBounds(140, 11, 160, 34);
        contentPanel.add(lblStatistiche);

        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setBounds(300, 45, 160, 140);
        contentPanel.add(lblNewLabel);
        lblNewLabel.setIcon(new ImageIcon("images/statistics.png"));
    }
}