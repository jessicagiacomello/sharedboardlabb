package Client;

import Server.RmiObservableService_server;
import Server.User;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.RemoteException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.Vector;

public class StatisticsForUsers extends JDialog {

    /**
     *
     */
    static JTable table;
    private User u;

    RmiObservableService_server remoteService;

    public StatisticsForUsers(RmiObservableService_server stub, User user, Date inizio, Date fine) throws SQLException, RemoteException {
        remoteService = stub;
        this.u = user;

        setIconImage(Toolkit.getDefaultToolkit().getImage("images/users.png"));
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setTitle("Statistiche Segmenti");
        setAutoRequestFocus(false);
        setResizable(false);

        Vector<String> headers = new Vector<>();
        headers.add("Lavagna");
        headers.add("Utente");
        headers.add("Segmenti");


        if (inizio == null && fine == null) {
            inizio = new Date(0);
            fine = new Date(System.currentTimeMillis());
        }

        Vector<Vector<String>> data = remoteService.GetUsersForStatistics(u, inizio, fine);

        final DefaultTableModel model = new DefaultTableModel(data, headers);

        table = new JTable(model);
        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        header_size();

        JScrollPane scroll = new JScrollPane(table);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        getContentPane().add(scroll, BorderLayout.EAST);
        pack();
        setVisible(true);

        JPanel underTop = new JPanel();

        JButton Cancel = new JButton("Indietro");
        Cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        Cancel.setBounds(100, 100, 100, 100);

        underTop.add(Cancel);
        getContentPane().add(underTop, BorderLayout.SOUTH);
    }

    /**
     * Setting the particular Column Size in JTable
     */
    public static void header_size() {
        TableColumn column = table.getColumnModel().getColumn(0);
        column.setPreferredWidth(100);
    }
}	 
