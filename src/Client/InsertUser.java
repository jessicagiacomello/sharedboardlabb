package Client;

import Server.ExistingUsernameException;
import Server.RmiObservableService_server;
import Server.User;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Vector;

public class InsertUser extends JDialog {

    /**
     *
     */
    private final JPanel contentPanel = new JPanel();
    private JTextField n;
    private JTextField c;
    private JTextField un;
    private JTextField p;
    private JTextField e;
    private String COLOR;

    RmiObservableService_server remoteUser;

    /**
     * Create the dialog.
     *
     * @throws RemoteException
     * @throws NotBoundException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public InsertUser(RmiObservableService_server stub) throws RemoteException, SQLException {
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/register.png"));

        remoteUser = stub;
        COLOR = "Nero";

        setTitle("Insert User");
        setBounds(100, 100, 450, 300);
        getContentPane().setLayout(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        getContentPane().add(contentPanel, BorderLayout.CENTER);
        contentPanel.setLayout(null);

        JLabel lblInserireNomeUtente = new JLabel("Nome");
        lblInserireNomeUtente.setBounds(24, 29, 124, 14);
        contentPanel.add(lblInserireNomeUtente);

        JLabel lblNewLabel = new JLabel("Cognome");
        lblNewLabel.setBounds(24, 65, 124, 14);
        contentPanel.add(lblNewLabel);

        JLabel lblInserireUsername = new JLabel("Username");
        lblInserireUsername.setBounds(24, 103, 124, 14);
        contentPanel.add(lblInserireUsername);

        JLabel lblInserirePassword = new JLabel("Password");
        lblInserirePassword.setBounds(24, 140, 124, 14);
        contentPanel.add(lblInserirePassword);

        JLabel lblInserireEmail = new JLabel("Email");
        lblInserireEmail.setBounds(24, 175, 124, 14);
        contentPanel.add(lblInserireEmail);

        JLabel lblColore = new JLabel("Colore");
        lblColore.setBounds(24, 206, 124, 14);
        contentPanel.add(lblColore);

        n = new JTextField();
        n.setBounds(221, 25, 168, 22);
        contentPanel.add(n);
        n.setColumns(10);

        c = new JTextField();
        c.setBounds(221, 61, 168, 22);
        contentPanel.add(c);
        c.setColumns(10);

        un = new JTextField();
        un.setBounds(221, 99, 168, 22);
        contentPanel.add(un);
        un.setColumns(10);

        p = new JPasswordField();
        p.setBounds(221, 136, 168, 22);
        contentPanel.add(p);
        p.setColumns(10);

        e = new JTextField();
        e.setBounds(221, 171, 168, 22);
        contentPanel.add(e);
        e.setColumns(10);

        Vector<String> v = remoteUser.ReturnColors();

        final JComboBox<String> colori = new JComboBox<>(v);

        colori.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent event) {
                if (event.getStateChange() == ItemEvent.SELECTED) {
                    COLOR = (String) colori.getSelectedItem();
                    System.err.println("Colore: " + COLOR);
                }
            }
        });

        colori.setSelectedItem(0);
        colori.setSelectedIndex(-1);
        colori.setBounds(221, 206, 168, 22);
        contentPanel.add(colori);
        {
            JPanel buttonPane = new JPanel();
            buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
            getContentPane().add(buttonPane, BorderLayout.SOUTH);
            {
                JButton okButton = new JButton("OK");
                okButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {

                        if (!(n.getText().equals("") && c.getText().equals("") && un.getText().equals("") && p.getText().equals("") && e.getText().equals(""))) {
                            User u = insertUser();

                            try {

                                try {
                                    remoteUser.save(u);
                                    JOptionPane.showMessageDialog(null, "Utente registrato " + n.getText() + " " + c.getText() + " " + "\n" + "Benvenuto/a in SharedBoard! ");
                                } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | ExistingUsernameException e) {
                                    JOptionPane.showMessageDialog(null, e.getMessage());
                                }
                                dispose();
                            } catch (RemoteException e) {
                                e.printStackTrace();
                            }

                        } else {
                            if (n.getText().equals(""))
                                JOptionPane.showMessageDialog(null, "ATTENZIONE!!!" + "\n" + "Inserire nome utente");
                            if (c.getText().equals(""))
                                JOptionPane.showMessageDialog(null, "ATTENZIONE!!!" + "\n" + "Inserire cognome utente");
                            if (un.getText().equals(""))
                                JOptionPane.showMessageDialog(null, "ATTENZIONE!!!" + "\n" + "Inserire username");
                            if (p.getText().equals(""))
                                JOptionPane.showMessageDialog(null, "ATTENZIONE!!!" + "\n" + "Inserire password");
                            if (e.getText().equals(""))
                                JOptionPane.showMessageDialog(null, "ATTENZIONE!!!" + "\n" + "Inserire e-mail");
                        }
                    }
                });
                okButton.setActionCommand("OK");
                buttonPane.add(okButton);
                getRootPane().setDefaultButton(okButton);
            }
            {
                JButton cancelButton = new JButton("Annulla");
                cancelButton.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent arg0) {
                        dispose();
                    }
                });
                cancelButton.setActionCommand("Annulla");
                buttonPane.add(cancelButton);
            }
        }
    }

    public User insertUser() {
        String name = n.getText();
        String surname = c.getText();
        String email = e.getText();
        String nickname = un.getText();
        String password = p.getText();
        String captcha = "";
        String color = COLOR;

        return new User(name, surname, nickname, password, email, captcha, color);
    }
}
