package Client;

import Server.RmiObservableService_server;
import Server.SendEmail;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.Vector;

public class InvitedPeople extends JDialog {

    /**
     *
     */
    static JTable table;
    static String user, email;
    int index;
    private String line1;
    private String line2;

    RmiObservableService_server remoteService;

    public InvitedPeople(RmiObservableService_server stub) throws SQLException {

        remoteService = stub;
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setTitle("Scegli utenti dal DB");
        setAutoRequestFocus(false);
        setResizable(false);

        JPanel topPanel = new JPanel();

        JLabel label1 = new JLabel("Elenco Utenti del sistema");
        label1.setPreferredSize(new Dimension(200, 30));

        // JLabel label2 = new JLabel("Tabella riferimento SQL: Utenti");
        // label2.setPreferredSize(new Dimension(200, 30));

        topPanel.add(label1);
        // topPanel.add(label2);

        getContentPane().add(topPanel, BorderLayout.NORTH);

        Vector<String> headers = new Vector<>();
        headers.add("Username");
        headers.add("Email");

        Vector<Vector<String>> data = new Vector<>();

        try {
            data = remoteService.GetAllUsersForInvited(Client_GUI.user);
        } catch (RemoteException e1) {
            e1.printStackTrace();
        }

        final DefaultTableModel model = new DefaultTableModel(data, headers);
        table = new JTable(model);
        table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent arg0) {
                index = table.getSelectedRow();
                if (index == -1) {
                    return;
                }
                email = table.getModel().getValueAt(index, 1).toString();
                System.err.println("Utente scelto: " + email);
                user = table.getModel().getValueAt(index, 0).toString();
                System.err.println("Utente scelto: " + user);
            }
        });

        table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        header_size();

        JScrollPane scroll = new JScrollPane(table);
        scroll.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        getContentPane().add(scroll, BorderLayout.EAST);
        pack();
        setVisible(true);

        line1 = "Gentile utente," + "\n";
        line2 = "Ti invito a partecipare alla mia lavagna! I dati per l'accesso sono: ";

        JPanel underTop = new JPanel();
        JButton Confirm = new JButton("Conferma");

        Confirm.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                try {
                    String inv_board = JOptionPane.showInputDialog("Inserire la lavagna a cui vuoi invitare l'utente", null);
                    String mail = JOptionPane.showInputDialog("Inserire email dell'utente:", email);
                    SendEmail se = new SendEmail(mail);
                    se.setSubject("Dati per accedere a lavagna");
                    String txt = line1 + line2 + inv_board;
                    se.setText(txt);
                    se.send();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                model.fireTableDataChanged();
                table.clearSelection();
            }
        });

        JButton Cancel = new JButton("Indietro");
        Cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        Confirm.setBounds(100, 100, 100, 100);
        Cancel.setBounds(100, 100, 100, 100);

        underTop.add(Confirm);
        underTop.add(Cancel);
        getContentPane().add(underTop, BorderLayout.SOUTH);
    }

    /**
     * Setting the particular Column Size in JTable
     */
    public static void header_size() {
        TableColumn column = table.getColumnModel().getColumn(0);
        column.setPreferredWidth(100);
        column = table.getColumnModel().getColumn(1);
        column.setPreferredWidth(100);
    }
} 
