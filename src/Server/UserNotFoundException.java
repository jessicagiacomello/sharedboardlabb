package Server;

/**
 * @author Jessica Giacomello, Nicolò Cardozo
 */

public class UserNotFoundException extends Exception {

    /**
     * Instantiate a new <code>UserNotFoundException</code>
     */
    public String msg;

    /**
     * @return A string representing the exception
     */
    public UserNotFoundException(String s) {
        msg = s;
    }
}
