package Server;

import Whiteboard.Board;
import Whiteboard.Segment;
import java.io.IOException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

public interface RmiObservableService_server extends Remote {

    void addObserver(RemoteObserver_client o) throws RemoteException;

    boolean login(String username, String password) throws NullPointerException, IOException, SQLException, UserNotFoundException, WrongPasswordException;

    User GetUser(String username) throws RemoteException;

    void save(User u) throws RemoteException, InstantiationException, IllegalAccessException, ClassNotFoundException, ExistingUsernameException;

    boolean confirm(String username, String password, String captcha) throws RemoteException;

    Vector<String> ReturnColors() throws RemoteException, SQLException;

    Vector<Vector<String>> GetAllUsersForInvited(User u) throws RemoteException, SQLException;

    Vector<Vector<String>> GetUsersForStatistics(User u, Date inizio, Date fine) throws RemoteException, SQLException;

    String InsertBoard(Board b) throws IOException;

    Vector<Vector<String>> getYourBoards(User u) throws RemoteException, SQLException;

    void deleteYourBoard(User u, String board) throws RemoteException, SQLException;

    boolean returnCaptcha(String cap, String username, String password) throws NullPointerException, IOException, SQLException;

    void InsertSegment(Segment s) throws RemoteException, SQLException;

    ArrayList<Segment> getSegments(String Board_name) throws RemoteException;

    int getSizeDB(String Board_name) throws RemoteException;

    Segment getLastSegment(String Board_name) throws RemoteException;

    void deleteSegment(int id) throws RemoteException;

    void NotifyRemovedElement(ArrayList<Segment> segments) throws RemoteException;

    void Notify_Refresh_clear(String board_name) throws RemoteException;

    Vector<Vector<String>> getActiveBoards(String username) throws RemoteException, SQLException;

    boolean checkExistingBoard(String boardName) throws RemoteException, SQLException;
}
