package Server;

/**
 * @author Jessica Giacomello, Nicolò Cardozo
 */

public class RandomString {

    /**
     * @param str
     */
    private String str;

    /**
     * method randomString create an alphanumerics String sent via Email by the server to confirm the account
     *
     */
    public RandomString() {
        String alphaNumerics = "qwertyuiopasdfghjklzxcvbnm1234567890";
        str = "";
        for (int i = 0; i < 8; i++) {
            str += alphaNumerics.charAt((int) (Math.random() * alphaNumerics.length()));
        }
    }

    /**
     * method that return the string of str
     *
     */
    public String toString() {
        return str;
    }
}
