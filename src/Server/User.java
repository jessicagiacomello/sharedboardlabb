package Server;

import java.io.Serializable;

public class User implements Serializable {

    /**
     *
     */
    private String name;
    private String surname;
    private String username;
    private String password;
    private String email;
    private String captcha;
    private String Color;

    public User() {
    }

    public User(String n, String s, String u, String p, String e, String c, String Col) {
        name = n;
        surname = s;
        username = u;
        password = p;
        email = e;
        captcha = c;
        Color = Col;
    }

    public String getPassword() {
        return password;
    }

    /**
     * Retrieves the username of this user
     *
     * @return the String representing the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Retrieves the user's name
     *
     * @return the user's name
     */
    public String getName() {
        return name;
    }

    /**
     * Retrieves the user's surname
     *
     * @return the user's surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Retrieves the user's email address
     *
     * @return user's email
     */
    public String getEmail() {
        return email;
    }

    public String getColor() {
        return Color;
    }
}