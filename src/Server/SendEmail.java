package Server;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

public class SendEmail {
    private final String host = "smtp.gmail.com";
    private final String from = "sharedboard2021@gmail.com";
    private final String ToAddress;
    private final String user = "sharedboard2021@gmail.com";
    private final String pass = "Sharedboard2021LabB";
    private MimeMessage msg = null;
    private Session session = null;

    public SendEmail(String toAddress) {
        ToAddress = toAddress;
        try {
            Properties props = System.getProperties();

            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", 587);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.user", user);
            props.put("mail.smtp.password", pass);
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.starttls.required", "true");
            props.put("mail.smtp.startssl.enable", "true");
            props.put("mail.smtp.startssl.required", "true");
            props.put("mail.debug", "true");

            session = Session.getDefaultInstance(props, null);
            session.setDebug(true);
            session.setPasswordAuthentication(new URLName("smtp", host, 25, "INBOX", user, pass), new PasswordAuthentication(user, pass));

            msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(from));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(ToAddress));
        } catch (MessagingException e) {
            System.out.println(e);
        }
    }

    public void send() {
        // Invio messaggio
        Transport tr;
        try {
            tr = session.getTransport("smtp");
            tr.connect(host, user, pass);
            msg.saveChanges();
            tr.sendMessage(msg, msg.getAllRecipients());
            tr.close();
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void setText(String txt) {
        try {
            msg.setText(txt);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void setSubject(String s) {
        try {
            msg.setSubject(s);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }
}
