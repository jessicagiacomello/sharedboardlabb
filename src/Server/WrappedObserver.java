package Server;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.Observable;
import java.util.Observer;

class WrappedObserver implements Observer, Serializable {

    private RemoteObserver_client ro;

    public WrappedObserver(RemoteObserver_client ro) {
        this.ro = ro;
    }

    @Override
    public void update(Observable o, Object arg) {
        try {
            ro.remoteUpdate(o.toString(), arg);
        } catch (RemoteException e) {
            o.deleteObserver(this);
        }
    }
}