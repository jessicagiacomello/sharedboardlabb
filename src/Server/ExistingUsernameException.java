package Server;

public class ExistingUsernameException extends Exception {

    /**
     *
     */
    private final String msg;

    /**
     * Instantiate a new <code>ExistingUsernameException</code>
     *
     * @param s
     */
    public ExistingUsernameException(String s) {
        super(s);
        msg = s;
    }

    /**
     * @return A string representing the exception
     */
    public String toString() {
        return msg;
    }
}