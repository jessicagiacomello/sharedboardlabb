package Server;

/**
 * @author Jessica Giacomello, Nicolò Cardozo
 */
public class WrongPasswordException extends Exception {
    public String msg;

    /**
     * Instantiates a new <code>WrongPasswordException</code>
     *
     * @param s
     */
    public WrongPasswordException(String s) {
        super(s);
        msg = s;
    }
}