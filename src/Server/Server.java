package Server;

import Whiteboard.Board;
import Whiteboard.Segment;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.sql.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Vector;

public class Server extends Observable implements RmiObservableService_server {

    static Connection con = null;
    Repository repository;

    public Server() {

        try {
            String url = "jdbc:mysql://localhost:3306/";
            String username = "root";
            String password = "04Verde6!";

            con = DriverManager.getConnection(url, username, password);
            Statement statement = con.createStatement();
            statement.execute("CREATE DATABASE IF NOT EXISTS sharedBoardDB");
            statement.execute("USE sharedBoardDB");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

        repository = new Repository();
    }

    public static void main(String[] args) {
        try {
            Registry registry = LocateRegistry.createRegistry(1099);
            Server obj = new Server();
            RmiObservableService_server stub = (RmiObservableService_server) UnicastRemoteObject.exportObject(obj, 0);

            registry.rebind("RmiService", stub);

            System.out.println("Server created.");
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void addObserver(RemoteObserver_client o) {
        WrappedObserver mo = new WrappedObserver(o);
        addObserver(mo);
    }

    @Override
    public void save(User u) throws ExistingUsernameException {
        repository.save(u);
    }

    @Override
    public boolean confirm(String username, String password, String captcha) {
        repository.confirm(username, password, captcha);
        return true;
    }

    @Override
    public boolean login(String username, String password) throws NullPointerException, SQLException, UserNotFoundException, WrongPasswordException {
        return repository.login(username, password);
    }

    @Override
    public Vector<String> ReturnColors() {
        return repository.ReturnColors();
    }

    @Override
    public Vector<Vector<String>> GetAllUsersForInvited(User u) {
        return repository.getAllUsersForInvited(u);
    }

    @Override
    public Vector<Vector<String>> GetUsersForStatistics(User u, Date inizio, Date fine) {
        return repository.getUsersForStatistics(u, inizio, fine);
    }

    @Override
    public User GetUser(String username) {
        return repository.GetUser(username);
    }

    @Override
    public String InsertBoard(Board b) {
        return repository.insertBoard(b);
    }

    @Override
    public Vector<Vector<String>> getYourBoards(User u) {
        return repository.getYourBoards(u);
    }

    @Override
    public void deleteYourBoard(User u, String board) {
        repository.deleteYourBoard(u, board);
    }

    public Vector<Vector<String>> getActiveBoards(String username) {
        return repository.getActiveBoards(username);
    }

    @Override
    public boolean returnCaptcha(String cap, String username, String password) throws NullPointerException, SQLException {
        return repository.returnCaptcha(cap, username, password);
    }

    @Override
    public void InsertSegment(Segment s) {
        int id = repository.insertSegment(s);
        s.setIdSegment(id);
        setChanged();
        notifyObservers(s);
    }

    public ArrayList<Segment> getSegments(String Board_name) {
        return repository.getSegments(Board_name);
    }

    public int getSizeDB(String Board_name) {
        return repository.getSizeDB(Board_name);
    }

    public Segment getLastSegment(String Board_name) {
        return repository.getLastSegment(Board_name);
    }

    @Override
    public void deleteSegment(int id) {
        repository.deleteSegment(id);
    }

    @Override
    public void NotifyRemovedElement(ArrayList<Segment> segments) {
        setChanged();
        notifyObservers(segments);
    }

    @Override
    public void Notify_Refresh_clear(String board_name) {
        setChanged();
        notifyObservers(board_name);
    }

    @Override
    public boolean checkExistingBoard(String boardName) {
        return repository.checkExistingBoard(boardName);
    }
}
