package Server;

import Whiteboard.Segment;
import java.util.ArrayList;

public class SegmentsList {
    private final ArrayList<Segment> listAll;
    private final ArrayList<Segment> listTemp;

    public SegmentsList() {
        super();
        listAll = new ArrayList<>();
        listTemp = new ArrayList<>();
    }

    public void reset_all() {
        listAll.clear();
    }

    public boolean add(Segment e) {
        if (!listTemp.isEmpty()) {
            listTemp.clear();
		}
		return listAll.add(e);
	}

    public boolean insertSegment(Segment e) {
        return listAll.add(e);
    }

    public boolean is_in_list(Segment s) {
        for (int i = 0; i < listAll.size(); i++) {
            if (listAll.get(i).getIdSegment() == s.getIdSegment())
                return true;
        }
        return false;
    }

    public int undo(String Username) {
        if (!listAll.isEmpty()) {
            Segment s = getLastElement(Username);
            if (s != null) {
                listTemp.add(s);
                return s.getIdSegment();
            } else return -1;
        }
        return -1;
    }

    public Segment redo() {
        if (!listTemp.isEmpty()) {
            Segment s = listTemp.remove(listTemp.size() - 1);
            return s;
        }
        return null;
    }

    public Segment getLastElement(String Username) {
        Segment s;
        for (int i = listAll.size() - 1; i >= 0; i--) {
            s = listAll.get(i);
            if (s.getUsername().equals(Username))
                return s;
        }
        return null;
    }

    public void removeSegment(int id) {
        for (int i = 0; i < listAll.size(); i++) {
            if (listAll.get(i).getIdSegment() == id) {
                listAll.remove(i);
                return;
            }
        }
    }

    public int size() {
    	return listAll.size();
    }

    public ArrayList<Segment> getList() {
        return listAll;
    }
}
