package Server;

import java.rmi.Remote;
import java.rmi.RemoteException;

// Interfaccia che serve all'observer per aggiornare i suoi comandi
public interface RemoteObserver_client extends Remote {
    void remoteUpdate(Object observable, Object updateMsg) throws RemoteException;
}