package Server;

import Whiteboard.Board;
import Whiteboard.Segment;
import java.sql.*;
import java.util.ArrayList;
import java.util.Vector;

// Contiene i metodi che utilizzano il database.
public class Repository {

    static Connection con = null;
    public static Vector<Vector<String>> data = new Vector<>();
    public static Vector<Vector<String>> myboards = new Vector<>();
    public static Vector<Vector<String>> boards = new Vector<>();

    public String random;
    public String username;

    public Repository() {
        try {
            String url = "jdbc:mysql://localhost:3306/";
            String username = "root";
            String password = "04Verde6!";

            con = DriverManager.getConnection(url, username, password);
            Statement statement = con.createStatement();
            statement.execute("CREATE DATABASE IF NOT EXISTS sharedBoardDB");
            statement.execute("USE sharedBoardDB");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public synchronized void save(User u) throws ExistingUsernameException {
        try {
            if (u == null) {
                throw new NullPointerException();
            }
            if (!this.UserNotFound(u.getUsername())) {
                throw new ExistingUsernameException("Username già in uso.");
            }

            String name = u.getName();
            String surname = u.getSurname();
            String username = u.getUsername();
            String password = u.getPassword();
            String email = u.getEmail();
            String captcha;
            String color = u.getColor();

            PreparedStatement newUser;
            random = new RandomString().toString();
            captcha = random;
            newUser = con.prepareStatement("INSERT INTO Utenti" + "(nome,cognome,username,password,email,captcha,color) VALUES(?,?,?,?,?,?,?);");
            newUser.setString(1, name);
            newUser.setString(2, surname);
            newUser.setString(3, username);
            newUser.setString(4, password);
            newUser.setString(5, email);
            newUser.setString(6, captcha);
            newUser.setString(7, color);
            newUser.executeUpdate();

            SendEmail se = new SendEmail(u.getEmail());
            se.setSubject("Conferma registrazione a Sharedboard");

            String line1 = "Gentile " + u.getName() + " " + u.getSurname() + ",\n";
            String line2 = "Benvenuto in LavagnaCondivisa!\n Inserisci la seguente sequenza di caratteri per confermare la tua iscrizione:";
            String line3 = "\n" + random;
            String txt = line1 + line2 + line3;
            se.setText(txt);
            se.send();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized boolean returnCaptcha(String captcha, String username, String password) throws NullPointerException, SQLException {
        ResultSet rs = null;
        try {
            PreparedStatement cap = con.prepareStatement("SELECT captcha FROM Utenti WHERE captcha = ? AND username = ? AND password = ?");
            cap.setString(1, captcha);
            cap.setString(2, username);
            cap.setString(3, password);

            rs = cap.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs.next();
    }

    /**
     * method to confirm the account of the user
     *
     * @param username
     * @param password
     * @return
     */

    public synchronized boolean confirm(String username, String password, String captcha) {
        try {
            PreparedStatement confirm = con.prepareStatement("UPDATE Utenti SET conferma = 's' WHERE username = ? AND password = ? AND captcha = ?");
            confirm.setString(1, username);
            confirm.setString(2, password);
            confirm.setString(3, captcha);
            confirm.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public synchronized boolean login(String username, String password) throws NullPointerException, SQLException, UserNotFoundException, WrongPasswordException {
        //verifica se l'utente è presente nel DB e fa fare l'autenticazione
        ResultSet rs = null;

        try {
            if (this.UserNotFound(username)) {
                throw new UserNotFoundException("The user " + username + " does not exists");
            }
            if (this.passwordNotFound(username, password)) {
                throw new WrongPasswordException("");
            }

            PreparedStatement log = con.prepareStatement("SELECT * FROM Utenti WHERE username=? AND password=? AND conferma = 's'", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            log.setString(1, username);
            log.setString(2, password);
            rs = log.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs.next();
    }

    public synchronized Vector<String> ReturnColors() {
        String query = ("SELECT colore FROM Colori");
        ResultSet rs;
        Statement st;

        try {
            st = con.createStatement();
            rs = st.executeQuery(query);
            Vector<String> v = new Vector<>();

            while (rs.next()) {
                String color = rs.getString("colore");
                v.add(color);
            }
            return v;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * method that search if the user is existing
     *
     * @param username
     * @return
     * @throws SQLException
     */

    public boolean UserNotFound(String username) throws SQLException {
        PreparedStatement find_user = con.prepareStatement("SELECT DISTINCT COUNT(username) AS ids FROM Utenti WHERE username = ?");
        find_user.setString(1, username);
        ResultSet rs = find_user.executeQuery();
        rs.next();

        return rs.getInt("ids") == 0;
    }

    /**
     * method that search if the user's password exists
     *
     * @param username
     * @param password
     * @return
     * @throws SQLException
     */

    public boolean passwordNotFound(String username, String password) throws SQLException {
        //se non trova la password dell'utente restituisce 0
        PreparedStatement user_password = con.prepareStatement("SELECT DISTINCT COUNT(username) AS pwd FROM Utenti WHERE username = ? AND password = ?", ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        user_password.setString(1, username);
        user_password.setString(2, password);
        ResultSet rs = user_password.executeQuery();
        rs.first();
        return rs.getInt("pwd") == 0;
    }


    public synchronized Vector<Vector<String>> getAllUsersForInvited(User u) {

        String str = "SELECT username,email FROM Utenti  WHERE username <> '" + u.getUsername() + "'";
        ResultSet rs;
        Statement st;
        data.removeAllElements();

        try {
            st = con.createStatement();
            rs = st.executeQuery(str);

            while (rs.next()) {
                Vector<String> d = new Vector<>();
                d.add(rs.getString("Username"));
                d.add(rs.getString("Email"));
                // d.add("\n\n\n\n\n\n\n");
                data.add(d);
            }
            return data;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public synchronized Vector<Vector<String>> getUsersForStatistics(User u, Date inizio, Date fine) {
        String str = "SELECT nome_lavagna as Lavagna, user as Utente, count(id_tratto) as Segmenti from lavagna " +
                "inner join tratto on nome_lav = nome_lavagna " +
                "where creatore_lavagna = '" + u.getUsername() + "' " +
                "and tratto.datacreazione BETWEEN '" + inizio + "' and '" + fine + "' " +
                "group by Utente, Lavagna " +
                "order by Lavagna ASC, Segmenti DESC";

        ResultSet rs;
        Statement st;
        data.removeAllElements();

        try {
            st = con.createStatement();
            rs = st.executeQuery(str);

            while (rs.next()) {
                Vector<String> d = new Vector<>();
                d.add(rs.getString("Lavagna"));
                d.add(rs.getString("Utente"));
                d.add(rs.getString("Segmenti"));
                data.add(d);
            }
            return data;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public synchronized User GetUser(String Username) {
        String str = "SELECT * FROM Utenti WHERE username = '" + Username + "'";
        ResultSet rs;
        Statement st;

        try {
            st = con.createStatement();
            rs = st.executeQuery(str);

            while (rs.next()) {
                String username = rs.getString("username");
                String name = rs.getString("nome");
                String surname = rs.getString("cognome");
                String email = rs.getString("email");
                String password = rs.getString("password");
                String captcha = rs.getString("captcha");
                String color = rs.getString("color");

                return new User(name, surname, username, password, email, captcha, color);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * method to insert the board into database
     *
     * @param board
     * @return
     */
    public synchronized String insertBoard(Board board) {

        try {
            if (checkExistingBoard(board.getBoardname())) {
                return "esistente";
            }

            String boardname = board.getBoardname();
            Date data = board.getCreation();
            String username = board.getUsername();
            PreparedStatement newBoard;
            newBoard = con.prepareStatement("INSERT INTO Lavagna" + "(nome_lavagna,datacreazione,creatore_lavagna) VALUES(?,?,?);");
            newBoard.setString(1, boardname);
            newBoard.setDate(2, data);
            newBoard.setString(3, username);
            newBoard.executeUpdate();

            return boardname;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * method that return all the boards that the user have been created
     *
     * @return
     */
    public synchronized Vector<Vector<String>> getYourBoards(User u) {
        String str = "SELECT nome_lavagna,datacreazione FROM Lavagna WHERE creatore_lavagna = '" + u.getUsername() + "'";
        ResultSet rs;
        Statement st;
        myboards.removeAllElements();

        try {
            st = con.createStatement();
            rs = st.executeQuery(str);

            while (rs.next()) {
                Vector<String> board = new Vector<>();
                board.add(rs.getString("nome_lavagna"));
                board.add(rs.getString("datacreazione"));
               // board.add("\n\n\n\n\n\n\n");
                myboards.add(board);
            }
            return myboards;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * method used by the user to delete his boards
     */
    public synchronized void deleteYourBoard(User u, String board) {
        String query = "DELETE FROM Lavagna WHERE creatore_lavagna = '" + u.getUsername() + "' AND nome_lavagna = '" + board + "'";
        Statement st;

        try {
            st = con.createStatement();
            st.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized int insertSegment(Segment sgmt) {
        ResultSet keys;
        int id;

        try {
            String sql = "INSERT INTO Tratto (datacreazione,nome_lav,user) VALUES('" + sgmt.getCreation() + "','" + sgmt.getBoardname() + "','" + sgmt.getUsername() + "');";

            PreparedStatement pstmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.executeUpdate();
            keys = pstmt.getGeneratedKeys();    // si ottiene l'id del segmeto generato
            keys.next();
            id = keys.getInt(1); // recupero l'id che è stato generato
            insertCoordinates(sgmt.getList(), id); // inserisco coordinate relative al segmento
            return id;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * method to insert the coordinates of the segment into database
     *
     * @param coordinates
     */
    public synchronized void insertCoordinates(ArrayList<Coordinates> coordinates, int idsegment) {

        PreparedStatement newCoordinates;
        String sql = "INSERT INTO Coordinate(x,y,id_tratto) VALUES(?,?,?);";

        try {
            con.setAutoCommit(false);
            newCoordinates = con.prepareStatement(sql);

            // per ogni coordinata popolo la tabella
            for (int i = 0; i < coordinates.size(); i++) {
                newCoordinates.setInt(1, coordinates.get(i).getX());
                newCoordinates.setInt(2, coordinates.get(i).getY());
                newCoordinates.setInt(3, idsegment);
                newCoordinates.addBatch();
            }

            newCoordinates.executeBatch();
            con.commit();
            newCoordinates.clearBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public synchronized ArrayList<Segment> getSegments(String Board_name) {
        String sql = "SELECT distinct * FROM Tratto natural join Utenti where nome_lav= '" + Board_name + "'and Tratto.user = Utenti.Username";

        Statement statement;
        ArrayList<Segment> segments = new ArrayList<>();
        segments.clear();

        try {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            Segment seg;

            while (rs.next()) {
                seg = createSegment(rs, Board_name);
                segments.add(getCoordinates(seg));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return segments;
    }

    public synchronized Segment getCoordinates(Segment seg) {
        String sql = "SELECT * FROM Coordinate where Coordinate.id_tratto=" + seg.getIdSegment();
        Statement statement;

        try {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                Coordinates xy = new Coordinates(rs.getInt("x"), rs.getInt("y"));
                seg.addCoordinates(xy);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return seg;
    }

    public synchronized Segment createSegment(ResultSet rs, String Board_name) {
        int idsegment = 0;
        String user = null;
        String color = null;
        Date creation = null;

        try {
            idsegment = rs.getInt("id_tratto");
            color = rs.getString("color");
            user = rs.getString("user");
            creation = rs.getDate("datacreazione");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Segment seg = new Segment(creation, Board_name, user, idsegment, color);
        return seg;
    }

    public synchronized int getSizeDB(String Board_name) {
        String sql = "SELECT count(*) FROM Tratto where nome_lav='" + Board_name + "'";
        Statement statement;

        try {
            statement = con.createStatement();
            ResultSet rs = statement.executeQuery(sql);
            rs.next();
            return rs.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public synchronized Segment getLastSegment(String Board_name) {
        String sql = "SELECT * FROM Tratto,Utenti where Tratto.user=Utenti.username AND Tratto.nome_lav= '" + Board_name + "'";
        Statement statement;

        try {
            statement = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            ResultSet rs = statement.executeQuery(sql);

            if (rs.last()) {
                int idsegment = rs.getInt("id_tratto");
                String color = rs.getString("color");
                String user = rs.getString("user");
                Date creation = rs.getDate("datacreazione");
                Segment seg = new Segment(creation, Board_name, user, idsegment, color);

                return getCoordinates(seg);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public synchronized void deleteSegment(int id_tratto) {
        String sql = "DELETE FROM Tratto WHERE id_tratto = '" + id_tratto + "'";
        Statement statement;

        try {
            statement = con.createStatement();
            statement.execute(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * method that return all active boards that are not the board of the user who is using the system
     *
     * @return
     */
    public synchronized Vector<Vector<String>> getActiveBoards(String username) {
        // restituisce le lavagne di tutti gli utenti del db
        String str = "SELECT nome_lavagna,datacreazione,creatore_lavagna, email FROM Lavagna Join Utenti on Utenti.username = Lavagna.creatore_lavagna WHERE creatore_lavagna <> '" + username + "'";

        ResultSet rs;
        Statement st;
        boards.removeAllElements();

        try {
            st = con.createStatement();
            rs = st.executeQuery(str);

            while (rs.next()) {
                Vector<String> b = new Vector<>();
                b.add(rs.getString("nome_lavagna"));
                b.add(rs.getString("datacreazione"));
                b.add(rs.getString("creatore_lavagna"));
                b.add(rs.getString("email"));
                boards.add(b);
            }
            return boards;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * method that return if the given board exists
     *
     * @return
     */
    public synchronized boolean checkExistingBoard(String boardName) {
        String str = "SELECT nome_lavagna FROM Lavagna WHERE nome_lavagna = '" + boardName + "'";
        ResultSet rs;
        Statement st;

        try {
            st = con.createStatement();
            rs = st.executeQuery(str);
            return rs.next();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return false;
        }
    }
}