package Whiteboard;

import Client.Client_GUI;
import Server.RmiObservableService_server;
import Server.SegmentsList;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;

public class WhiteBoardClient extends WhiteBoard implements ActionListener {

    /**
     *
     */
    public WhiteBoardClient(String Boardname, RmiObservableService_server ru) throws IOException {
        this.stub = ru;
        Board_name = Boardname;
        setTitle("Nome Lavagna: " + Board_name + ", Username: " + Client_GUI.user.getUsername());
        this.List_Client_Segment = new SegmentsList();
    }

    protected void initUI() throws IOException {
        paper = new PaintPanel(this);
        super.initUI();
        Undo.addActionListener(this);
        Redo.addActionListener(this);
        paper.setVisible(true);
    }


    @Override
    public void actionPerformed(ActionEvent arg0) {
        String name = arg0.getActionCommand();

        if (name.equals("Undo")) {
            int idsegment = List_Client_Segment.undo(Client_GUI.user.getUsername());
            if (idsegment != -1) {
                List_Client_Segment.removeSegment(idsegment);
                paper.reDrawScreen(List_Client_Segment.getList());
            }
        }

        if (name.equals("Redo")) {
            Segment s = List_Client_Segment.redo();
            if (s != null) {
                List_Client_Segment.insertSegment(s);
                paper.reDrawScreen(List_Client_Segment.getList());
            }
        }
    }

    public synchronized void receiveSegment(Segment segm, boolean draw) {

        if (draw) {
            if (segm.username.equals(Client_GUI.user.getUsername())) { //il controllo non lo faccio su porte ma sullo username
                if (!List_Client_Segment.is_in_list(segm)) { //Se il Segmento è già in lista non va aggiunto se però non compare lo si aggiunge
                    List_Client_Segment.add(segm);
                    paper.drawSegment(segm);
                }
            } else {
                if (!List_Client_Segment.is_in_list(segm)) {
                    paper.drawSegment(segm);
                    List_Client_Segment.insertSegment(segm);
                }
            }
        } else
            List_Client_Segment.add(segm);
    }

    public synchronized void receiveSegments(ArrayList<Segment> segm) {
        paper.resetScreen();
        List_Client_Segment.reset_all();

        for (Segment s : segm) {
            List_Client_Segment.add(s);
        }

        paper.reDrawScreen(segm);
    }

    public synchronized void reset_board() {
        paper.resetScreen();
    }

    public String getBoardName() {
        return Board_name;
    }
}
