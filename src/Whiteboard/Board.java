package Whiteboard;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

public class Board implements Serializable {
    /**
     *
     */

    private Date creation;// data creazione lavagna
    private ArrayList<Segment> segments;// lista di tutti i segmenti presenti in essa
    public String whiteboardname;//nomelavagna
    private String username;//nome del creatore

    public Board(String boardname, Date creation, String user) {
        this.whiteboardname = boardname;
        this.creation = creation;
        this.username = user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getBoardname() {
        return whiteboardname;
    }

    public Date getCreation() {
        return creation;
    }
}
	
	