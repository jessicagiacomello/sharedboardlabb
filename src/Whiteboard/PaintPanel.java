package Whiteboard;

import Client.Client_GUI;
import Server.Coordinates;
import Server.User;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;


public class PaintPanel extends JPanel implements MouseMotionListener, Serializable, MouseListener {

    private static final Color COLOR_BACKGROUND = Color.WHITE;
    private static final int SIZE = 1000; // Size of paint area.
    private int x_a = 10, y_a = 10;
    public String col;
    public Color c;
    private Graphics2D g2;  // elemento per il disegno
    public Segment temp;        // buffer dove manteniamo i segmenti non ancora disegnati
    private BufferedImage _bufImage = null; // elemento disegno bufferizzato
    private String Board_name;
    private WhiteBoard wb;
    private User u;


    /*costruisce il pannello dove si disegna
     * prende un elemento whiteboard cosi da capire se � di tipo sender o listener
     */
    public PaintPanel(WhiteBoard wb) {
        u = Client_GUI.user;
        temp = new Segment(Board_name);//inizializzo il buffer di segmenti
        setPreferredSize(new Dimension(SIZE, SIZE));
        addMouseMotionListener(this);//settano il listener su questo elemento
        addMouseListener(this);
        setBackground(Color.WHITE); //colore sfondo pannello di disegno
        setLayout(null);
        this.wb = wb;
    }

    @Override
    public void paintComponent(Graphics g) { //componente base per il disegno di default
        super.paintComponent(g);
        g2 = (Graphics2D) g;

        if (_bufImage == null) {
            _bufImage = (BufferedImage) this.createImage(SIZE, SIZE);
            Graphics2D gc = _bufImage.createGraphics();
            gc.setColor(COLOR_BACKGROUND);
            gc.fillRect(0, 0, SIZE, SIZE);
        }
        g2.drawImage(_bufImage, null, 0, 0);
    }


    //questo è il metodo che effettivamente si occupa di disegnare sulla lavagna
    private Coordinates drawCurrentShape(Graphics2D g2, int x_b, int y_b, String color) {
        selectColor(color);
        g2.setColor(c);    // imposto il colore per disegnare
        g2.drawLine(x_a, y_a, x_b, y_b);
        return new Coordinates(x_b, y_b); //ritorno le coordinate del punto appena disegnato
    }

    public void selectColor(String color) {
        col = color;
        switch (col) {
            case "Nero":
                c = Color.BLACK;
                break;
            case "Giallo":
                c = Color.YELLOW;
                break;
            case "Rosso":
                c = Color.RED;
                break;
            case "Blue":
                c = Color.BLUE;
                break;
            case "Verde":
                c = Color.GREEN;
                break;
            case "Rosa":
                c = Color.PINK;
                break;
            case "Arancione":
                c = Color.ORANGE;
                break;
            case "Magenta":
                c = Color.MAGENTA;
                break;
            case "Viola":
                c = new Color(66, 49, 137);
                break;
            case "Grigio":
                c = Color.GRAY;
                break;
            case "Marrone":
                c = new Color(150, 75, 0);
                break;
            case "Ciano":
                c = Color.CYAN;
                break;
            case "Oro":
                c = new Color(255, 215, 0);
                break;
            case "Platino":
                c = new Color(229, 228, 226);
                break;
            case "Argento":
                c = new Color(192, 192, 192);
                break;
            case "Bronzo":
                c = new Color(205, 127, 50);
                break;
            case "Smeraldo":
                c = new Color(0, 152, 116);
                break;
            case "Zaffiro":
                c = new Color(8, 37, 103);
                break;
            case "Rubino":
                c = new Color(65, 0, 18);
                break;
            default:
                break;
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) { // all'evento mouse trascinato scatta
        if (x_a > -1 && y_a > -1) {
            int x_b = e.getX();
            int y_b = e.getY();//prendo coordinata  nuova b
            temp.addCoordinates(drawCurrentShape(_bufImage.createGraphics(), x_b, y_b, u.getColor())); //disegno nuova coordinata
            x_a = x_b; //coordinata b diventa coordiata a
            y_a = y_b;
        } else { // se siamo al primo punto premuto
            x_a = e.getX();
            y_a = e.getY();
        }
        repaint();
    }

    public void drawSegment(Segment seg) { // chiamato da wB_server o client e permette il disegno dei segmenti RICEVUTI
        if (_bufImage == null) {
            _bufImage = (BufferedImage) this.createImage(SIZE, SIZE);
            Graphics2D gc = _bufImage.createGraphics();
            gc.setColor(COLOR_BACKGROUND);
            gc.fillRect(0, 0, SIZE, SIZE);
        }

        int x_b, y_b;
        String color = seg.getColor();

        x_a = seg.getCoordinate(0).getX();// coordinate di partenza rispetto l ultima disegnata
        y_a = seg.getCoordinate(0).getY();

        for (int i = 1; i < seg.size(); i++) { //per ogni coordinata del segmento ricevuto
            x_b = seg.getCoordinate(i).getX(); // stesso passaggio prendo i due punti e disegno passando il colore
            y_b = seg.getCoordinate(i).getY();
            drawCurrentShape(_bufImage.createGraphics(), x_b, y_b, color);
            x_a = x_b;
            y_a = y_b;
            repaint();//aggiornamento pannello
        }
    }

    public void reDrawScreen(ArrayList<Segment> segments) {
        resetScreen();
        if (segments != null & !segments.isEmpty()) {
            for (Segment segment : segments) {
                drawSegment(segment);
                x_a = -1;
                y_a = -1;
            }
        }
    }

    public void resetScreen() {
        Graphics2D gc = _bufImage.createGraphics();
        gc.setColor(COLOR_BACKGROUND);
        gc.fillRect(0, 0, SIZE, SIZE);
        repaint();
    }

    public void mouseReleased(MouseEvent arg0) {
        Date date;
        date = new Date(System.currentTimeMillis());

        temp.setCreation(date);
        temp.setUsername(u.getUsername());
        temp.setColor(u.getColor());
        temp.setBoardname(wb.Board_name);

        try {
            wb.forwardSegment(temp);
        } catch (IOException | SQLException e1) {
            e1.printStackTrace();
        }
        temp = new Segment(Board_name);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        x_a = -1; //inizializiamo a -1 per sapere qual è il primo punto del segmento
        y_a = -1;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }
}
		