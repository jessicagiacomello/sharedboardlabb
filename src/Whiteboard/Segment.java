package Whiteboard;

import Server.Coordinates;
import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;

public class Segment implements Serializable {

    /**
     *
     */
    private ArrayList<Coordinates> coordinates;
    private int idSegment;
    private String Boardname;
    public String color;
    public String username;
    public Date creationTime;

    public Segment(Date creationTime, String Board, String user, int id, String color) {
        this.coordinates = new ArrayList<>();
        this.creationTime = creationTime;
        this.Boardname = Board;
        this.username = user;
        this.idSegment = id;
        this.color = color;
    }

    // questo costruttore vuoto serve a poter aggiungere le coordinate all'array all'inizio
    public Segment(String NameBoard) {
        this.coordinates = new ArrayList<>();
        this.Boardname = NameBoard;
    }

    public void addCoordinates(Coordinates c) {
        coordinates.add(c);
    }

    public Coordinates getCoordinate(int pos) {
        return coordinates.get(pos);
    }

    public int size() {
        return coordinates.size();
    }

    public ArrayList<Coordinates> getList() {
        return coordinates;
    }

    public String getBoardname() {
        return Boardname;
    }

    public void setBoardname(String boardname) {
        Boardname = boardname;
    }

    public Date getCreation() {
        return creationTime;
    }

    public void setCreation(Date d) {
        this.creationTime = d;
    }

    public int getIdSegment() {
        return idSegment;
    }

    public void setIdSegment(int idSegment) {
        this.idSegment = idSegment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String user) {
        this.username = user;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}