package Whiteboard;

import Client.Client_GUI;
import Server.RmiObservableService_server;
import Server.SegmentsList;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class WhiteBoard extends JFrame {

    /**
     *
     */
    // Panels
    public PaintPanel paper;
    protected JPanel underPanel;
    protected JPanel topPanel;

    // Undo & Redo
    protected JButton Undo, Redo;
    public SegmentsList List_Client_Segment;
    public ArrayList<Segment> List_Host_Segment;

    // Stub & User
    public RmiObservableService_server stub;
    public String Board_name;

    public WhiteBoard() throws IOException {
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/drawing.png"));
        System.out.println("User => " + Client_GUI.user.getUsername());
        initUI();
    }

    protected void initUI() throws IOException {
        setTitle(Board_name);
        topPanel = new JPanel();
        underPanel = new JPanel();

        Undo = new JButton("Undo");
        Undo.setBounds(100, 100, 100, 100);
        Undo.setActionCommand("Undo");
        topPanel.add(Undo);

        Redo = new JButton("Redo");
        Redo.setBounds(100, 100, 100, 100);
        Redo.setActionCommand("Redo");
        topPanel.add(Redo);


        JLabel Date = new JLabel("new label");
        Date.setBounds(100, 100, 100, 100);
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy");
        Date now = new Date(System.currentTimeMillis());
        String strDate = sdfDate.format(now);
        Date.setText(strDate);
        topPanel.add(Date);

        JLabel time = new JLabel("new label");
        time.setBounds(100, 100, 100, 100);
        Calendar cal = Calendar.getInstance();
        cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        String times = sdf.format(cal.getTime());
        time.setText(times);
        topPanel.add(time);

        getContentPane().add(paper);

        setSize(850, 650);
        getContentPane().add(underPanel, BorderLayout.SOUTH);

        getContentPane().add(topPanel, BorderLayout.NORTH);
    }

    public void forwardSegment(Segment segm) throws IOException, SQLException {
        stub.InsertSegment(segm); // inserisce il segmento facendo 'partire' lo scambio dei segmenti
    }

    public String getBoard_name() {
        return Board_name;
    }

    public void receiveSegment(Segment temp, boolean draw) {
    }

    public void receiveSegments(ArrayList<Segment> tmp) {
    }

    public void reset_board() {
    }
}

