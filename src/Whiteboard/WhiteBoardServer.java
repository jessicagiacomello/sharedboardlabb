package Whiteboard;

import Client.Client_GUI;
import Server.RmiObservableService_server;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.util.ArrayList;


public class WhiteBoardServer extends WhiteBoard implements ActionListener {

    public WhiteBoardServer(Board board, RmiObservableService_server ru) throws IOException {
        this.stub = ru;
        Board_name = board.getBoardname();
        this.List_Host_Segment = new ArrayList<>();

        setTitle("Nome Lavagna: " + Board_name + ", Username: " + Client_GUI.user.getUsername() + " [Creatore Lavagna]");
    }

    public void initUI() throws IOException {
        paper = new PaintPanel(this);
        super.initUI();
        paper.setVisible(true);
        Undo.addActionListener(this);
        Redo.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        String name = arg0.getActionCommand();

        if (name.equals("Undo")) {
            int x;

            try {
                x = stub.getSizeDB(Board_name);

                if (x > 0) {
                    Segment s = stub.getLastSegment(Board_name);
                    if (s != null) {
                        List_Host_Segment.add(s);
                        int id = s.getIdSegment();
                        if (id != -1) {
                            stub.deleteSegment(id);
                            // notifica ai client che ho tolto un segmento inviando loro la nuova lista di segmenti
                            ArrayList<Segment> ar = stub.getSegments(Board_name);
                            if (ar.isEmpty()) {
                                stub. Notify_Refresh_clear(Board_name);
                                paper.resetScreen();
                            } else {
                                stub.NotifyRemovedElement(ar);
                                paper.reDrawScreen(stub.getSegments(Board_name));
                            }
                        }
                    }
                }
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        if (name.equals("Redo")) {
            Segment s = null;
            if (!List_Host_Segment.isEmpty())
                s = List_Host_Segment.remove(List_Host_Segment.size() - 1);
            if (s != null) {
                try {
                    stub.InsertSegment(s);
                    paper.reDrawScreen(stub.getSegments(Board_name));
                } catch (RemoteException | SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized void receiveSegment(Segment segm, boolean draw) {
        if (draw) {
            paper.drawSegment(segm);
        }
    }

    public synchronized void receiveSegments(ArrayList<Segment> segm) {
        paper.reDrawScreen(segm);
    }

    public synchronized void reset_board() {
        paper.resetScreen();
    }
}